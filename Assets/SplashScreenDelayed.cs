﻿using UnityEngine;
using System.Collections;

public class SplashScreenDelayed : MonoBehaviour {

	public float delayTime = 3;
	
	IEnumerator Start()
	{
		yield return new WaitForSeconds (delayTime);

		UnityEngine.SceneManagement.SceneManager.LoadScene (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
	}
}
