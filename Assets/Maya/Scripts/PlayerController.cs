﻿using UnityEngine;
using System.Collections;
//Adding this allows us to access members of the UI namespace including Text.
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public float speed;
	public Text countText;          //Store a reference to the UI Text component which will display the number of pickups collected.
	public Text winText;            //Store a reference to the UI Text component which will display the 'You win' message.
	//public Text timer;

	bool facingRight = true;
	bool up = false;
	bool down = false;
	bool pressE = false;
	bool play = false;
	GameObject pickup = null;

	private Rigidbody2D rb2d;
	//private Vector2 previous;
	private int count;              //Integer to store the number of pickups collected so far.
	private float timer = 60f;
	private AudioSource audioSource;


	Animator anim;

	void Start ()
	{
		rb2d = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		audioSource = GetComponent<AudioSource> ();

		count = 0;
		winText.text = "";
		//timer.ToString ("0");

		//Call our SetCountText function which will update the text with the current value for count.
		SetCountText ();
	}


	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		if (moveVertical != 0 && moveHorizontal != 0) {
			return;
		}

		anim.SetFloat ("Speed", Mathf.Abs (moveHorizontal));
		//anim.SetFloat ("vSpeed", rb2d.velocity.y);

		if (moveVertical > 0.0f) {
			anim.SetBool ("Up", true);
		} else if (moveVertical < 0.0f) {
			anim.SetBool ("Down", true);
		} else {
			anim.SetBool ("Down", false);
			anim.SetBool ("Up", false);
		}
	
		Vector2 movement = new Vector2 (moveHorizontal, moveVertical);

		rb2d.velocity = movement * speed;

		//if (Input.anyKey) {
		//	rb2d.position = new Vector2 (rb2d.position.x + 0.1f, rb2d.position.y + 0.1f);
		//}
		if (moveHorizontal > 0 && !facingRight)
			Flip ();
		else if (moveHorizontal < 0 && facingRight)
			Flip (); 

		if (pressE && Input.GetKeyDown (KeyCode.E)) {
			pickup.SetActive (false);
			pressE = false;
			audioSource.Play ();
			count++;

			//Update the currently displayed count by calling the SetCountText function.
			SetCountText ();
		}
	}

	void Update ()
	{
		timer -= Time.deltaTime;

		if (timer <= 0)
			timer = 0;
	}

	void OnGUI ()
	{
		GUI.Box(new Rect(715, 10, 50, 20), "" + timer.ToString("0"));
		if (pressE == true) {
			GUI.Label (new Rect (Camera.main.WorldToScreenPoint (rb2d.transform.position).x, 
		                         Camera.main.WorldToScreenPoint (rb2d.transform.position).y, 
				                 50, 20), 
				                 "Press E to Pick up ");
		}
	}

	void Flip ()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		//Check the provided Collider2D parameter other to see if it is tagged "PickUp", if it is...
		if (other.gameObject.CompareTag("PickUp"))
		{
			pressE = true;
			pickup = other.gameObject;
		}
	}

	void OnTriggerExit2D (Collider2D other)
	{
		//Check the provided Collider2D parameter other to see if it is tagged "PickUp", if it is...
		if (other.gameObject.CompareTag ("PickUp")) {
			pressE = false;
		}
	}


//This function updates the text displaying the number of objects we've collected and displays our victory message if we've collected all of them.
	void SetCountText()
	{
		//Set the text property of our our countText object to "Count: " followed by the number stored in our count variable.
		countText.text = "Count: " + count.ToString ();

		if (count >= 22)
			//... then set the text property of our winText object to "You win!"
			winText.text = "You win!";
	}
}